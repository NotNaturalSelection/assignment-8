C_SRC_K = -g -c

.GO: all

all:
	rm -rf bin
	mkdir bin
	gcc $(C_SRC_K) writebmp.c -o bin/writebmp.o
	gcc $(C_SRC_K) readbmp.c -o bin/readbmp.o
	gcc $(C_SRC_K) bmp.c -o bin/bmp.o
	gcc $(C_SRC_K) sepia_sse.c -o bin/sepia_sse.o
	nasm -felf64 -g sepia_asm.asm -o bin/sepia_asm.o
	gcc $(C_SRC_K) main.c -o bin/main.o
	gcc -g bin/*.o -o bin/main
	rm -rf bin/*.o
