//
// Created by notnaturalselection on 18.12.2020.
//

#ifndef ASSIGNMENT_8_SEPIA_SSE_H
#define ASSIGNMENT_8_SEPIA_SSE_H
#include "bmp.h"
#include <stddef.h>
#include <stdlib.h>

void sepia_sse(struct image *in, struct image *out);
#endif //ASSIGNMENT_8_SEPIA_SSE_H
