#include <stdio.h>
#include <stdlib.h>
#include "writebmp.h"
#include "readbmp.h"
#include "bmp.h"
#include "sepia_sse.h"


int main() {
    struct bmp_info *bmp = new_bmp();
    char *filename = "large.bmp";
    char new_filename[15];
    FILE *source = fopen(filename, "r");
    sprintf(new_filename, "sepia.bmp");
    FILE *dest = fopen(new_filename, "w");
    from_bmp(source, bmp);

//    bmp->img = sepia(bmp->img);
    struct image *res = malloc(sizeof(struct image));
    sepia_sse(bmp->img, res);
    bmp->img = res;

    to_bmp(dest, bmp);
}
