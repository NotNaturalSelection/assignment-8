#ifndef UNTITLED_READBMP_H
#define UNTITLED_READBMP_H

#include <stdbool.h>
#include <stdint.h>

const uint16_t BF_TYPE1;
const uint16_t BF_TYPE2;

enum read_status {
    READ_OK = 0,
    READ_INVALID_BIT_MAP_FILE_HEADER,
    READ_INVALID_BIT_COUNT,
    READ_INVALID_HEADER
};

struct bit_map_file_header *read_bit_map_file_header(FILE *in);

bool is_bit_map_file_header_valid(struct bit_map_file_header *bmfh);

struct bit_map_info_core *read_bit_map_info_core(FILE *in);

void read_pixels(FILE *in, struct image *pix_buf);

enum read_status from_bmp(FILE *in, struct bmp_info *bmp);

#endif //UNTITLED_READBMP_H
